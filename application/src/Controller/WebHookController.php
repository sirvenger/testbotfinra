<?php

namespace App\Controller;

use App\Entity\Chat;
use App\Telegram\Telegram;
use App\Telegram\TelegramException;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebHookController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/setWebhook")
     * @throws Exception|TransportExceptionInterface
     */
    public function setWebhook(): Response
    {
        $botToken      = $this->getParameter('app.bot_token');
        $url_hook      = $this->getParameter('app.bot_hook');
        $urlSetWebhook = Telegram::API_BASE_URI . $botToken . '/setWebhook';

        $response = $this->client->request('GET', $urlSetWebhook, [
            'query' => [
                'url' => $url_hook . '/handler'
            ]
        ]);

        return new Response(
            '<html><body>'.$response->getContent().'</body></html>'
        );
    }

    /**
     * @Route("/handler")
     * @throws Exception
     */
    public function handle(LoggerInterface $logger): Response
    {
        $botToken = $this->getParameter('app.bot_token');
        $telegram = new Telegram($botToken, $this->client, 'BandReddit');
        $repositoryChat = $this->getDoctrine()->getRepository(Chat::class);
        $entityManager = $this->getDoctrine()->getManager();

        try {
            $chat = $telegram->handle();

            if ($chat === null) {
                return new Response();
            }

            $chat_model = $repositoryChat->findOneBy(['chat_id' => $chat->getChatId()]);

            if ($chat_model !== null) {
                return new Response();
            }

            $entityManager->persist($chat);
            $entityManager->flush();

            $telegram->sendMessage(Telegram::GREETING, $chat->getChatId());
            $telegram->sendPhoto(Telegram::URL_PHOTO, $chat->getChatId());
            $telegram->sendPoll($chat->getChatId(), Telegram::QUESTION);
        } catch (TransportExceptionInterface|TelegramException $e) {
            $logger->error($e->getMessage());
        }

        return new Response();
    }
}