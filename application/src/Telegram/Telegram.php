<?php

namespace App\Telegram;

use App\Entity\Chat;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Telegram
{
    public const API_BASE_URI = 'https://api.telegram.org/bot';
    public const GREETING = 'Добрый день!';
    public const QUESTION = 'Вы инвестируете?';
    public const ACTION_SEND_MESSAGE = 'sendMessage';
    public const ACTION_SEND_PHOTO = 'sendPhoto';
    public const ACTION_SEND_POLL = 'sendPoll';
    public const MY_CHAT_MEMBER = 'my_chat_member';
    public const CHAT = 'chat';
    public const URL_PHOTO = 'https://habrastorage.org/getpro/moikrug/uploads/company/100/007/326/1/logo/d77aaba5596f1ae831b47d066b8363fe.jpg';
    public const OPTION_QUESTION = [
        'да',
        'постоянно',
        'нет',
        'иногда'
    ];

    /**
     * @var string
     */
    protected $api_key = '';

    /**
     * @var string
     */
    protected $bot_username = '';

    /**
     * @var int
     */
    protected $bot_id = 0;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * Telegram constructor.
     *
     * @param string $api_key
     * @param string $bot_username
     * @param HttpClientInterface $client
     * @throws TelegramException
     */
    public function __construct(string $api_key, HttpClientInterface $client, string $bot_username = '')
    {
        if (empty($api_key)) {
            throw new TelegramException('API KEY not defined!');
        }

        preg_match('/(\d+):[\w\-]+/', $api_key, $matches);
        if (!isset($matches[1])) {
            throw new TelegramException('Invalid API KEY defined!');
        }

        $this->bot_id  = (int) $matches[1];
        $this->api_key = $api_key;

        $this->bot_username = $bot_username;
        $this->client = $client;
    }

    /**
     * Handle bot request from webhook my_chat_member
     *
     * @return Chat|null
     *
     * @throws TelegramException
     */
    public function handle(): ?Chat
    {
        if ($this->bot_username === '') {
            throw new TelegramException('Bot Username is not defined!');
        }

        $input = file_get_contents('php://input');
        if (empty($input)) {
            throw new TelegramException('Input is empty! The webhook must not be called manually, only by Telegram.');
        }

        $post = json_decode($input, true);
        if (empty($post)) {
            throw new TelegramException('Invalid input JSON! The webhook must not be called manually, only by Telegram.');
        }

        if (isset($post[self::MY_CHAT_MEMBER])) {
            $chat = new Chat();
            $chat->setChatId((int)$post[Telegram::MY_CHAT_MEMBER]['chat']['id']);
            $chat->setTitle($post[Telegram::MY_CHAT_MEMBER]['chat']['title']);

            return $chat;
        }

        return null;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendMessage(string $message, int $chat_id)
    {
        $data = [
            'chat_id' => $chat_id,
            'text' => $message,
        ];

        $url = Telegram::API_BASE_URI . $this->api_key . '/' . self::ACTION_SEND_MESSAGE;

        $this->client->request('POST', $url, [
           'body' => $data,
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendPhoto(string $url_photo, int $chat_id)
    {
        $data = [
            'chat_id' => $chat_id,
            'photo' => $url_photo,
        ];

        $url = Telegram::API_BASE_URI . $this->api_key . '/' . self::ACTION_SEND_PHOTO;

        $this->client->request('POST', $url, [
            'body' => $data,
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendPoll(int $chat_id, string $question)
    {
        $data = [
            'chat_id' => $chat_id,
            'question' => $question,
            'options' => json_encode(self::OPTION_QUESTION)
        ];

        $url = Telegram::API_BASE_URI . $this->api_key . '/' . self::ACTION_SEND_POLL;

        $this->client->request('POST', $url, [
            'body' => $data,
        ]);
    }
}