<?php

namespace App\Telegram;

use Exception;

/**
 * Main exception class used for exception handling
 */
class TelegramException extends Exception
{

}
